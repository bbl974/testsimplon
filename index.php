<?php
error_reporting(0);
session_start();
require_once("include/fct.inc.php");
require_once("include/class.pdogsb.inc.php");
include("vues/v_entete.php") ;


$pdo = PdoGsb::getPdoGsb();
$estConnecte = estConnecte();
if (!isset($_REQUEST['uc']) || !$estConnecte) {
	$_REQUEST['uc'] = 'connexion';
}
$uc = $_REQUEST['uc'];
switch ($uc) {
	case 'connexion': {
			include("controleurs/c_connexion.php");
			break;
		}
	case 'gestion_visiteurs': {
			include("controleurs/c_GestionVisiteur.php");
			break;
		}
	case 'gestion_ordinateurs': {
			include("controleurs/c_GestionOrdinateur.php");
			break;
		}
	case 'gestion_attributions': {
			include("controleurs/c_GestionAttribution.php");
			break;
		}
		
}
?>


